/*
 * Copyright (c) 2019. Botanická zahrada Přírodovědecké fakulty Univerzity Karlovy, Juraj Šimon
 */

import Vuex from "vuex";
import modules from "./stores";

export default new Vuex.Store({
  modules
});
