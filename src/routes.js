/*
 * Copyright (c) 2019. Botanická zahrada Přírodovědecké fakulty Univerzity Karlovy, Juraj Šimon
 */

import Vue from "./init";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    { path: "/", redirect: "/search" },
    {
      path: "/search",
      name: "home",
      component: () => import("./components/pages/home.vue")
    },
    {
      path: "/diversity",
      name: "diversity",
      component: () => import("./components/pages/diversity")
    }
  ]
});
