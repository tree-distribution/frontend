import axios from "axios";
import urlJoin from "proper-url-join";

export default {
  async getById(id) {
    let response = await axios.get(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `plants/${id}`)
    );
    return response.data;
  }
};
