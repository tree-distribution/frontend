import axios from "axios";
import urlJoin from "proper-url-join";

export default {
  async get() {
    let response = await axios.get(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `map-data`)
    );
    return response.data;
  }
};
