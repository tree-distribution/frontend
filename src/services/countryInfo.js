import axios from "axios";
import urlJoin from "proper-url-join";

export default {
  async get(code) {
    let response = await axios.get(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `country/${code}`)
    );
    return response.data;
  },
  async getAllDiversity() {
    let response = await axios.get(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `countries`)
    );
    return response.data;
  }
};
