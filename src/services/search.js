import axios from "axios";
import urlJoin from "proper-url-join";

export default {
  async search(query, page, rowsPerPage) {
    const params = new URLSearchParams();
    params.append("q", query);
    params.append("page", page);
    params.append("rowsPerPage", rowsPerPage);
    let response = await axios.get(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "search"),
      {
        params
      }
    );
    return response.data;
  }
};
