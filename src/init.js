import Vue from "vue";
import Vuex from "vuex";
import VueAnalytics from "vue-analytics";

Vue.use(VueAnalytics, {
  id: "UA-88593536-2"
});
Vue.use(Vuex);

export default Vue;
