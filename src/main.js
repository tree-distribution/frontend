import Vue from "./init";
import App from "./components/app";
import vuetify from "./plugins/vuetify";
import router from "./routes";
import store from "./store";

import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader
import "./styles/index.scss";

Vue.config.productionTip = false;

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount("#app");
