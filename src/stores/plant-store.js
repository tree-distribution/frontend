/*
 * Copyright (c) 2019 Eva Novotná, Juraj Šimon
 */
import countryInfoService from "./../services/countryInfo";
import familyService from "./../services/family";

const MUTATIONS = {
  SET_PLANT: "setPlant",
  CLEAR_PLANT: "clearPlant",
  SET_COUNTRY: "setCountry",
  CLEAR_COUNTRY: "clearCountry",
  SET_FAMILY: "setFamily",
  CLEAR_FAMILY: "clearFamily",
  ADD_COUNTRY_INFO: "addCountryInfo",
  ADD_FAMILY_INFO: "addFamilyInfo"
};

export default {
  namespaced: true,
  state: {
    plant: null,
    country: null,
    countryInfo: {},
    familyInfo: {},
    family: null
  },
  getters: {
    plant: state => state.plant,
    country: state => state.country,
    family: state => state.family
  },
  mutations: {
    [MUTATIONS.SET_PLANT]: (state, plant) => {
      state.plant = plant;
    },
    [MUTATIONS.CLEAR_PLANT]: state => {
      state.plant = null;
    },
    [MUTATIONS.SET_COUNTRY]: (state, countryCode) => {
      state.country = countryCode;
    },
    [MUTATIONS.CLEAR_COUNTRY]: state => {
      state.country = null;
    },
    [MUTATIONS.SET_FAMILY]: (state, family) => {
      state.family = family;
    },
    [MUTATIONS.CLEAR_FAMILY]: state => {
      state.family = null;
    },
    [MUTATIONS.ADD_COUNTRY_INFO]: (state, info) => {
      state.countryInfo[info.id] = info;
    },
    [MUTATIONS.ADD_FAMILY_INFO]: (state, info) => {
      state.familyInfo[info.id] = info;
    }
  },
  actions: {
    setPlant({ commit }, plant) {
      commit(MUTATIONS.SET_PLANT, plant);
    },
    clearPlant({ commit }) {
      commit(MUTATIONS.CLEAR_PLANT);
    },
    setCountry({ commit }, countryCode) {
      commit(MUTATIONS.SET_COUNTRY, countryCode);
    },
    clearCountry({ commit }) {
      commit(MUTATIONS.CLEAR_COUNTRY);
    },
    setFamily({ commit }, family) {
      commit(MUTATIONS.SET_FAMILY, family);
    },
    clearFamily({ commit }) {
      commit(MUTATIONS.CLEAR_FAMILY);
    },
    async getCountryInfo({ commit, state }, countryCode) {
      if (state.countryInfo[countryCode]) {
        return state.countryInfo[countryCode];
      }
      let info = await countryInfoService.get(countryCode);
      commit(MUTATIONS.ADD_COUNTRY_INFO, info);
      return info;
    },
    async getFamilyInfo({ commit, state }, id) {
      if (state.familyInfo[id]) {
        return state.familyInfo[id];
      }
      let info = await familyService.getById(id);
      commit(MUTATIONS.ADD_FAMILY_INFO, info);
      return info;
    }
  }
};
